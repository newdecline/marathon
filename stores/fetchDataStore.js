import {BaseStore , getOrCreateStore} from 'next-mobx-wrapper';
import {observable, action} from "mobx";

class FetchDataStore extends BaseStore {
    @observable data = {};
    @action fetchData(data) {
        this.data = data
    }
}

export const fetchDataStore = getOrCreateStore('fetchData', FetchDataStore);