import {BaseStore , getOrCreateStore} from 'next-mobx-wrapper';
import {observable, action} from "mobx";

class AppStore extends BaseStore {
    @observable isOpenMenu = false;
    @observable isOpenLoginForm = false;
    @action setIsOpenMenu(value) {
        if (this.isOpenLoginForm) {
            this.setIsOpenLoginForm()
        }
        if (value !== false) {
            return this.isOpenMenu = !this.isOpenMenu
        }
        this.isOpenMenu = value
    }
    @action setIsOpenLoginForm() {
        if (this.isOpenMenu) {
            this.setIsOpenMenu(false)
        }
        this.isOpenLoginForm = !this.isOpenLoginForm
    }
}

export const appStore = getOrCreateStore('appStore', AppStore);