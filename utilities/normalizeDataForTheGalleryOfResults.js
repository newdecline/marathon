export const normalizeDataForTheGalleryOfResults = data => {
    return data.map(result => {
        return {
            avatar: result.avatar.cropped && result.avatar.cropped.url,
            weightStart: result.weight_before,
            weightEnd: result.weight_after,
            weightDifference: result.weight_before - result.weight_after,
            photosWithResults: result.gallery.gallerySlides.map(img => img.image.cropped && img.image.cropped.url)
        }
    });
};