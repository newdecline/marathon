export const normalizeDataForTheGalleryOfFeedbacks = data => {
    return data.map(review => {
        return {
            avatar: review.avatar.cropped && review.avatar.cropped.url,
            imgSrc: review.gallery.gallerySlides.map(img => img.image.cropped && img.image.cropped.url),
            name: review.name,
            text: review.text
        }
    });
};