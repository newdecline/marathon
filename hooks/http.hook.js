import {useState, useCallback} from 'react';

export const useHttp = () => {
    const [loading, setLoading] = useState(false);

    const request = useCallback(async (url, customOptions = {}) => {
        setLoading(true);

        const options = {
            query: {},
            method: 'get',
            ...customOptions
        };

        let urlObject = '';

        if (process.browser) {
            urlObject = new URL(`/api${url}`, window.location.origin);
        } else {
            urlObject = new URL(`/api${url}`, `${process.env.BASE_URL}`);
        }

        for (const key in options.query) {
            urlObject.searchParams.append(key, options.query[key]);
        }

        const response = await fetch(urlObject.href, options);
        const data = await response.json();

        setLoading(false);

        return {
            headers: response.headers,
            status: {status: response.status, ok: response.ok},
            data
        };
    }, []);

    return {
        loading,
        request
    }
};