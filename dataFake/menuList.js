export const menuList = [
    {
        href: '/',
        as: '/',
        anchorId: 'main',
        label: 'Главная'
    },
    {
        href: '/',
        as: '/',
        anchorId: 'about',
        label: 'О проекте'
    },
    {
        href: '/',
        as: '/',
        anchorId: 'participation-results',
        label: 'Результаты участниц'
    },
    {
        href: '/',
        as: '/',
        anchorId: 'feedback',
        label: 'Отзывы'
    }
];