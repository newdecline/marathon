import styled from 'styled-components';

export const StyledPhotosResultsPopup = styled('div')`
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    overflow: auto;
    z-index: 999;
    .close {
        padding: 6px 9px;
        display: flex;
        align-items: center;
        background: #e5e5e5;
        border-radius: 15px;
        border: 0;
        box-sizing: border-box;
        font-family: 'Circe-300', sans-serif;
        &__icon {
            display: flex;
            margin-right: 12px;
        }
        &__text {
            font-size: 16px;
            line-height: 120%;
            color: #454545;
        }
    }
    .card-wrap {
        position: relative;
        margin: 0;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        &__body {
            display: flex;
            align-items: center;
        }
    }
    .card {
        width: 263px;
        display: flex;
        flex-direction: column;
        padding: 20px;
        margin: 0 0 33px 0;
        background-color: #fff;
        box-sizing: border-box;
        &__img-wrap {
            position: relative;
            padding-top: 133%;
            margin-bottom: 25px;
            display: flex;
            align-items: center;
            justify-content: center;
            img {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                object-fit: contain;
            }
        }   
    }
    .footer {
        margin-top: auto;
        display: flex;
        align-items: baseline;
        color: #757575;
        font-size: 20px;
        line-height: 120%;
        &__difference {
            margin-right: 22px;
            font-size: 36px;
            line-height: 100%;
            font-family: 'Bodoni-400';
        }
        &__would {
            position: relative;
            width: 73px;
            font-size: 20px;
            &-icon {
                display: flex;
                width: 5px;
                height: auto;
                position: absolute;
                bottom: 7px;
                left: 55px;
            }
            &::after {
                content: '';
                position: absolute;
                bottom: 11px;
                left: -14px;
                display: block;
                width: 100%;
                height: 1px;
                background-color: #757575;
            }
        }
        &__has-become {
            font-size: 20px;
        }
    }
    .prev-btn, .next-btn {
        display: flex;
        margin: 0;
        padding: 0;
        border: none;
        align-self: center;
        background-color: transparent;
        transition: opacity .3s;
        &:disabled {
            opacity: .3;
        }
        svg {
            width: 20px;
            height: auto;
        }
    }
    .prev-btn {
        margin-right: 20px;
    }
    .next-btn {
        margin-left: 20px;
        transform: rotate(180deg);
    }
    @media (orientation: landscape) and (max-width: 1023px) {
        display: block;
        .card-wrap {
            padding: 50px 0;
        }
    }
    @media (min-width: 1024px) {
        .card {
            display: flex;
            flex-direction: column;
            padding: 30px;
            width: 297px;
        }
        .footer {
            margin-top: auto;
            display: flex;
            align-items: baseline;
            color: #757575;
            font-size: 20px;
            line-height: 120%;
            &__difference {
                font-size: 40px;
            }
            &__would {
                font-size: 20px;
                line-height: 120%;
            }
        }
    
        .prev-btn, .next-btn, .close{
            &:hover {
                cursor: pointer;
            }
        }
        .prev-btn {
            margin-right: 42px;
        }
        .next-btn {
            margin-left: 42px;
        }
    }
    @media (min-width: 1280px) {
        .card {
            width: 343px;
        }
        .footer {
            &__difference {
                font-size: 40px;
            }
            &__would {
                font-size: 20px;
                line-height: 120%;
            }
        }
    }
    @media (min-width: 1920px) {
        .card {
            width: 375px;
        }
    }
`;