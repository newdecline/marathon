import React from 'react';
import {useRouter} from 'next/router';
import {menuList} from "../../dataFake/menuList";
import scrollIntoView from "scroll-into-view";
import {observer, inject} from 'mobx-react';
import {Styled} from "./styled";
import InstagramIcon from "../../svg/instagram.svg";

export const NavigationList = inject('appStore', 'fetchDataStore')(observer(props => {
    const {
        appStore, fetchDataStore: {
            data: {
                layout: {instagram_url},
                topSection: {instagram_tag}
            }
        }
    } = props;

    const router = useRouter();

    const isOpenMenu = appStore.isOpenMenu;

    const handleClickMenuItem = (anchorId) => {
        appStore.setIsOpenMenu(false);
        const target = document.getElementById(anchorId);

        if (router.pathname !== '/') {
            router.push(`/#${anchorId}`);
        } else {
            scrollIntoView(target, {
                time: 1000,
                align: {
                    top: 0,
                    left: 0,
                    leftOffset: 0,
                    topOffset: 100
                }
            }, () => {
                window.location.href = `${window.location.pathname}#${anchorId}`;
                window.scrollBy(0, -100);
            });
        }
    };

    const renderLinks = () => {
        return menuList.map(item => (
            <li key={item.anchorId}
                onClick={() => handleClickMenuItem(item.anchorId)}
                className='navigation-list__item'>{item.label}</li>)
        )
    };

    return (
        <Styled isOpen={isOpenMenu}>
            <ul className='navigation-list'>
                {renderLinks()}
                <a href={instagram_url} target='_blank' rel='noopener noreferrer'
                   className="instagram-link">
                    <span className="instagram-link__svg-wrap"><InstagramIcon/></span>
                    <span className="instagram-link__text">{instagram_tag}</span>
                </a>
            </ul>
        </Styled>
    )
}));



