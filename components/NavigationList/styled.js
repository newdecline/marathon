import styled from "styled-components";

export const Styled = styled('nav')`
    .navigation-list {
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: calc(var(--vh, 1vh) * 100);
        padding: 110px 32px 32px 32px;
        display: flex;
        flex-direction: column;
        background: #000;
        box-sizing: border-box;
        transform: ${({isOpen}) => isOpen ? 'translate3d(0, 0, 0)' : 'translate3d(-100%, 0, 0)'};
        transition: transform .3s;
        &__item {
            margin-bottom: 40px;
            font-size: 20px;
            line-height: 120%;
            text-transform: uppercase;
            &:hover {
                cursor: pointer;
            }
            &:last-child {
                a {
                    text-decoration: none;
                    color: #E5E5E5;
                }
            }
        }
    }
    .instagram-link {
        display: flex;
        align-items: center;
        position: absolute;
        bottom: 50px;
        left: 32px;
        letter-spacing: 0.1em;
        color: #fff;
        text-decoration: none;
        font-size: 20px;
        line-height: 120%;
        &__svg-wrap {
            display: flex;
            margin-right: 40px;
        }
    }
    @media (min-width: 1024px) {
        .navigation-list {
            position: static;
            transform: unset;
            height: auto;
            padding: 0;
            flex-direction: row;
            background-color: transparent;
            &__item {
                margin-bottom: 0;
                margin-right: 47px;
                &:last-child {
                    margin-right: 0;
                }
            }
        }
        .instagram-link {
            display: none;
        }
    }
`;