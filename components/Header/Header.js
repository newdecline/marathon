import React, {useEffect, useState} from 'react';
import {enablePageScroll, disablePageScroll} from 'scroll-lock';
import LoginIcon from '../../svg/log-in.svg';
import {NavigationList} from "../NavigationList/NavigationList";
import {observer, inject} from 'mobx-react';
import {StyledBurgerMenu, StyledHeader} from "./styled";
import {LoginForm} from "../LoginForm/LoginForm";

export const Header = inject('appStore', 'fetchDataStore')(observer(props => {

    const {
        appStore,
        fetchDataStore: {
            data: {
                layout: {whats_app_url}
            }
        }
    } = props;

    const isOpenMenu = appStore.isOpenMenu;
    useEffect(() => {
        if (isOpenMenu) {
            disablePageScroll();
        } else {
            enablePageScroll();
        }
    }, [isOpenMenu]);

    const [isExpandHeightHeader, setIsExpandHeightHeader] = useState(false);

    useEffect(() => {
        window.addEventListener('scroll', handleScrollPage);
        handleScrollPage();
        return () => window.removeEventListener('scroll', handleScrollPage)
    }, []);

    const handleClickBurgerBtn = () => {
        appStore.setIsOpenMenu();
    };

    const handleScrollPage = () => {
        const $header = document.querySelector('.fixed-position-header');
        $header.style.top = '0';
        $header.style.position = 'fixed';

        const scrollStop = function (callback) {
            if (!callback || typeof callback !== 'function') return;
            let isScrolling;
            window.addEventListener('scroll', function (event) {
                window.clearTimeout(isScrolling);
                isScrolling = setTimeout(function() {
                    callback();
                }, 300);
            }, false);
        };

        scrollStop(function () {
            // event scroll end
            $header.style.top = `${window.pageYOffset}px`;
            $header.style.position = 'absolute';
        });

        if (window.pageYOffset > 69) {
            setIsExpandHeightHeader(true);
        } else {
            setIsExpandHeightHeader(false);
        }
    };

    const handleClickLoginBtn = () => {
        if (localStorage.getItem('token')) {
            window.location.href = '/client';
        } else {
            appStore.setIsOpenLoginForm();
        }
    };

    return (
        <StyledHeader className='fixed-position-header' isExpandHeightHeader={isExpandHeightHeader}>
            <StyledBurgerMenu open={isOpenMenu} onClick={handleClickBurgerBtn}>
                <span className='item'/>
                <span className='item'/>
                <span className='item'/>
            </StyledBurgerMenu>

            <NavigationList/>

            <ul className='additional-menu'>
                <li className='additional-menu__item'>
                    <a target='_blank' rel='noopener noreferrer' href={whats_app_url}>Оплатить участие</a>
                </li>
                <li className='additional-menu__item' onClick={handleClickLoginBtn}>
                    <span className='svg-wrap'><LoginIcon/></span>
                    Войти
                </li>
            </ul>
            <LoginForm isExpandHeightHeader={isExpandHeightHeader}/>
        </StyledHeader>
    )
}));

