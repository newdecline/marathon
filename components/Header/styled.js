import styled from "styled-components";

export const StyledHeader = styled('header')`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    padding: ${({isExpandHeightHeader}) => isExpandHeightHeader ? '22px 22px 22px 35px' : '37px 22px 37px 35px'};
    display: flex;
    justify-content: space-between;
    background: rgba(0, 0, 0, .8);
    color: #E5E5E5;
    box-sizing: border-box;
    transition: padding .3s;
    z-index: 13;
    -webkit-transform: translate3d(0, 0, 0);
    .additional-menu {
        display: flex;
        z-index: 1;
        &__item {
            display: flex;
            margin-right: 49px;
            align-items: center;
            font-size: 20px;
            line-height: 120%;
            text-transform: uppercase;
            .svg-wrap {
                display: inline-flex;
                width: 26px;
                height: 26px;
                padding-right: 15px;
                transform: translateY(-2px);
                svg {
                  display: block;
                  height: 100%;
                }
            }
            a {
                text-decoration: none;
                color: #E5E5E5;
            }
            &:hover {
                cursor: pointer;
            }
            &:first-child {
                display: none;
            }
            &:last-child {
                margin-right: 0;
            }
        }
    }
    @media (min-width: 1024px) {
        padding: ${({isExpandHeightHeader}) => isExpandHeightHeader ? '22px 35px 22px 35px' : '37px 35px 37px 35px'};
    }
    @media (min-width: 1280px) {
        padding: ${({isExpandHeightHeader}) => isExpandHeightHeader ? '22px 81px 22px 90px' : '37px 81px 37px 90px'};
        .additional-menu {
            &__item {
                &:first-child {
                    display: block;
                }
            }
        }
    }
    @media (min-width: 1920px) {
        padding: ${({isExpandHeightHeader}) => isExpandHeightHeader ? '22px 81px 22px 70px' : '37px 81px 37px 70px'};
    }
`;

export const StyledBurgerMenu = styled('div')`
    display: inline-flex;
    flex-direction: column;
    justify-content: space-between;
    width: 38px;
    z-index: 1;
    .item {
        display: block;
        width: 100%;
        height: 4px;
        margin-bottom: 6px;
        background: #E5E5E5;
        transform-origin: center;
        transition: width .3s, transform .3s;
        &:first-child {
            transform: ${({open}) => open ? 'rotate(45deg) translate3d(8px, 8px, 0)' : 'rotate(0)'};
        }
        &:nth-child(2) {
            width: ${({open}) => open ? '0%' : '100%'};
        }
        &:last-child {
            transform: ${({open}) => open ? 'rotate(-45deg) translate3d(8px, -8px, 0)' : 'rotate(0)'};
            margin-bottom: 0;
        }
    }
    @media (min-width: 1024px) {
        display: none;
    }
`;