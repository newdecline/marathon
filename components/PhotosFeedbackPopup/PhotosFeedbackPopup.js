import React, {useState, useEffect, forwardRef} from 'react';
import PropTypes from 'prop-types'
import {StyledPhotosFeedbackPopup} from "./styled";
import CrossIcon from "../../svg/cross.svg";
import NarrowArrowIcon from "../../svg/narrow-arrow.svg";

export const PhotosFeedbackPopup = forwardRef((props, ref) => {
    const {
        onCloseFeedbackPhotos,
        isShowFeedbackPhotos,
        allPhotos,
        initialIndex} = props;

    const [currentParticipant, setCurrentParticipant] = useState(initialIndex);
    const [currentIndex, setCurrentIndex] = useState(0);

    const [reachEnd, setReachEnd] = useState(false);
    useEffect(() => {
        if (currentParticipant === allPhotos.length - 1
            && currentIndex + 1 > allPhotos[currentParticipant].length - 1) {
            setReachEnd(true);
        } else {
            setReachEnd(false);
        }
    }, [currentParticipant, allPhotos, currentIndex]);

    const [reachStart, setReachStart] = useState(true);
    useEffect(() => {
        if (currentParticipant === 0 && currentIndex === 0) {
            setReachStart(true);
        } else {
            setReachStart(false);
        }
    }, [currentParticipant, allPhotos, currentIndex]);

    const onPrevPhoto = () => {
        if (currentParticipant === 0 && currentIndex - 1 < 0) {
            return;
        }
        if (currentIndex - 1 < 0) {
            setCurrentIndex(allPhotos[currentParticipant - 1].length - 1);
            setCurrentParticipant(currentParticipant - 1);
        } else {
            setCurrentIndex(currentIndex - 1);
        }
    };

    const onNextPhoto = () => {
        if (currentParticipant === allPhotos.length - 1
            && currentIndex + 1 > allPhotos[currentParticipant].length - 1) {
            return;
        }
        if (currentIndex + 1 > allPhotos[currentParticipant].length - 1) {
            setCurrentIndex(0);
            setCurrentParticipant(currentParticipant + 1);
        } else {
            setCurrentIndex(currentIndex + 1);
        }
    };

    return (
        <>
            {isShowFeedbackPhotos && <div className="overlay"/>}
            <StyledPhotosFeedbackPopup ref={ref}>

                <div className="card-wrap">
                    <button disabled={reachStart} onClick={onPrevPhoto} className="prev-btn"><NarrowArrowIcon/></button>
                    <div className="card">
                        <div className="card__img-wrap">
                            <img onClick={onNextPhoto} src={allPhotos[currentParticipant][currentIndex]} alt="img"/>
                        </div>
                    </div>
                    {<button disabled={reachEnd} onClick={onNextPhoto} className="next-btn"><NarrowArrowIcon/></button>}
                </div>

                <button
                    onClick={onCloseFeedbackPhotos}
                    className='close'>
                    <CrossIcon className='close__icon'/>
                    <span className='close__text'>скрыть фото отзыва</span>
                </button>

            </StyledPhotosFeedbackPopup>
        </>

    )
});

PhotosFeedbackPopup.propTypes = {
    onCloseFeedbackPhotos: PropTypes.func,
    isShowFeedbackPhotos: PropTypes.bool,
    allPhotos: PropTypes.array,
    initialIndex: PropTypes.number
};