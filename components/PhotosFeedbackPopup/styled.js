import styled from 'styled-components';

export const StyledPhotosFeedbackPopup = styled('div')`
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    overflow: auto;
    z-index: 999;
    .close {
        padding: 6px 9px;
        display: flex;
        align-items: center;
        background: #e5e5e5;
        border-radius: 15px;
        border: 0;
        box-sizing: border-box;
        font-family: 'Circe-300', sans-serif;
        &__icon {
            display: flex;
            margin-right: 12px;
        }
        &__text {
            font-size: 16px;
            line-height: 120%;
            color: #454545;
        }
    }
    .card-wrap {
        position: relative;
        margin: 0;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .card {
        width: 263px;
        display: flex;
        flex-direction: column;
        padding: 25px;
        margin: 0 0 33px 0;
        background-color: #fff;
        box-sizing: border-box;
        &__img-wrap {
            position: relative;
            padding-top: 133%;
            display: flex;
            align-items: center;
            justify-content: center;
            img {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                object-fit: contain;
            }
        }   
    }
    .prev-btn, .next-btn {
        display: flex;
        margin: 0;
        padding: 0;
        border: none;
        align-self: center;
        background-color: transparent;
        transition: opacity .3s;
        &:disabled {
            opacity: .3;
        }
        svg {
            width: 20px;
            height: auto;
        }
    }
    .prev-btn {
        margin-right: 20px;
    }
    .next-btn {
        margin-left: 20px;
        transform: rotate(180deg);
    }
    @media (orientation: landscape) and (max-width: 1023px) {
        padding: 50px 0;
        justify-content: unset;
    }
    @media (min-width: 1024px) {
        .card {
            width: 320px;  
        }
        .footer {
            margin-top: auto;
            display: flex;
            align-items: baseline;
            color: #757575;
            font-size: 20px;
            line-height: 120%;
            &__difference {
                margin-right: 30px;
                font-size: 40px;
                line-height: 100%;
                font-family: 'Bodoni', sans-serif;
            }
            &__would {
                position: relative;
                padding-right: 60px;
                &-icon {
                    display: flex;
                    width: 5px;
                    height: auto;
                    position: absolute;
                    bottom: 7px;
                    left: 81px;
                }
                &::after {
                    content: '';
                    position: absolute;
                    bottom: 11px;
                    left: -14px;
                    display: block;
                    width: 99px;
                    height: 1px;
                    background-color: #757575;
                }
            }
            &__has-become {
                
            }
        }
    
        .prev-btn, .next-btn, .close{
            &:hover {
                cursor: pointer;
            }
        }
        .prev-btn {
            margin-right: 42px;
        }
        .next-btn {
            margin-left: 42px;
        }
    }
    @media (min-width: 1280px) {
        .card {
            width: 390px;  
        }
    }
    @media (min-width: 1920px) {
        .card {
            width: 529px;  
        }
    }
`;