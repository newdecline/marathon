import styled from "styled-components";

export const StyledAboutProjectSection = styled('section')`
    padding: 54px 35px 25px 35px;
    background-color: #e5e5e5;
    background-image: ${({backgroundImage}) => backgroundImage ? `url("${backgroundImage}")` : 'none'};
    background-repeat: no-repeat;
    background-position-x: right;
    background-position-y: top;
    background-size: auto 42%;
    box-sizing: border-box;
    .section-title {
        display: none;
    }
    .section-item-wrap {
        display: flex;
        flex-direction: column;
    }
    .section-item {
        margin-bottom: 35px;
        display: flex;
        flex-direction: column;
        &:nth-child(1) {
            order: 2;
            margin-bottom: 27px;
            .text-wrap {
                display: flex;
                flex-direction: column;
                margin-bottom: 10px;
                order: 1;
            }
            .img-wrap {
                order: 2;
            }
            .text {
                white-space: pre-wrap;
                order: 2;
            }
            .title {
                white-space: pre-wrap;
                order: 1;
                margin: 0 0 25px 0;
            }
        }
        &:nth-child(2) {
            order: 1;
            .text-wrap {
                order: 1;
            }
            .title {
                white-space: pre-wrap;
            }
            .text {
                white-space: pre-wrap;
            }
            .img-wrap {
                order: 2;
            }
        }
    }
    .title {
        margin: 62px 0 25px 0;
        font-family: 'Bodoni-400';
        font-size: 36px;
        line-height: 100%;
        color: #454545;
    }
    .text {
        margin-bottom: 18px;
        font-size: 20px;
        line-height: 120%;
        color: #454545;
    }
    @media (min-width: 1024px) {
        position: relative;
        display: flex;
        padding: 49px 54px 25px 64px;
        background-size: cover;
        .section-title {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            font-size: 20px;
            line-height: 120%;
            color: #454545;
            text-transform: uppercase;
            transform: rotate(-90deg) translate3d(-80px,-22px,0);        
        }
        .section-item-wrap {
            flex-direction: row;
        }
        .section-item {
            width: 49.5%;
            box-sizing: border-box;
            &:nth-child(1) {
                order: 1;
                margin-bottom: 0;
                .text-wrap {
                    order: 2;
                    width: 86%;
                }
                .img-wrap {
                    order: 1;
                    margin-bottom: 52px;
                    width: 49.5%;
                    box-sizing: border-box;
                }
                .title {
                    margin: 0 0 41px 0;
                }
            }
            &:nth-child(2) {
                order: 2;
                margin-bottom: 0;
                .title {
                     margin-top: 0;
                     margin-bottom: 33px;
                }
                .text {
                    margin-bottom: 20px;
                }
            }
        }
        .title {
             font-size: 46px;   
        }
    }
    @media (min-width: 1280px) {
        padding: 78px 44px 28px 131px;
        .section-title {
            transform: rotate(-90deg) translate3d(-110px, 32px, 0);
        }
        .section-item {
            width: 46.4%;
            &:nth-child(1) {
                .img-wrap {
                    width: 49.9%;
                    margin-bottom: 64px;
                } 
                .title-br:last-child {
                    display: none;
                }
                .title {
                    margin: 0 0 31px 0;
                }
            }
            &:nth-child(2) {
               .title {
                    margin-bottom: 42px;
                    margin-top: 44px;
                }
            }
        }
    }
    @media (min-width: 1920px) {
        padding: 101px 210px 64px 309px;
        .section-title {
            transform: rotate(-90deg) translate3d(-142px,11px,0);
        }
        .section-item:nth-child(2) {
            .title {
                margin-bottom: 59px;
                margin-top: 57px;
            }
        }
        .section-item:nth-child(1) {
            .title {
               margin: 0 0 48px 0;
            }
        }
    }
`;

export const StyledBannerSection = styled('section')`
    position: relative;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin-top: -110px;
    min-height: calc(var(--vh, 1vh) * 100);
    background: ${({bg}) => `#000 url(${bg}) no-repeat`};
    background-size: cover;
    background-position: center;
    overflow-x: hidden;
    .subtitle {
        position: absolute;
        top: calc(50% - 43px);
        left: 50%;
        transform: translate(-50%, -50%);
        width: 100%;
        margin: 0;
        font-family: 'Circe-700';
        font-size: 14px;
        line-height: 120%;
        text-align: center;
        letter-spacing: 0.5em;
        text-transform: uppercase;
        color: #fff;
        z-index: 1;
    }
    .title {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        margin: 0;
        width: 100%;
        font-size: 30px;
        line-height: 110%;
        text-align: center;
        color: #FFF;
        font-family: 'Bodoni-400';
        text-transform: uppercase;
        z-index: 1;
    }
    .author {
        position: absolute;
        top: calc(50% + 61px);
        left: 50%;
        transform: translate(-50%, -50%);
        width: 250px;
        max-width: 100%;
        height: 59px;
        margin-top: -10px;
        z-index: 1;
    }
    .event-link {
        display: none;
        &__svg-wrap svg {
            transform: rotate(90deg);
        }
    }
    .overlay {
        position: absolute;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.35);
        z-index: 0;
        pointer-events: none;
    }
    @media (min-width: 1024px) {
        .subtitle {
            top: calc(50% - 85px);
            font-size: 20px;
        }
        .title {
            font-size: 80px;
        }
        .author {
            top: calc(50% + 130px);
            width: 595px;
            height: 138px;
        }
        .event-link {
            display: flex;
            align-items: center;
            position: absolute;
            bottom: 201px;
            left: -104px;
            color: #fff;
            text-decoration: none;
            transform: rotate(-90deg);
            &__text {
                font-size: 20px;
                line-height: 120%;
                letter-spacing: 0.1em;
            }
            &__svg-wrap {
                display: flex;
                width: 21px;
                height: 21px;
                margin-right: 40px;
                svg {
                    width: 100%;
                    height: 100%;
                }
            }
        }
    }
    @media (min-width: 1280px) {
        .event-link {
            left: -61px;
        }
    }
    @media (min-width: 1920px) {
        .subtitle {
            top: calc(50% - 79px);
        }
        .event-link {
            left: -74px;
        }
        .title {
            font-size: 100px;
        }
        .author {
            top: calc(50% + 141px);
            width: 704px;
            height: 160px;
        }
    }
`;

export const StyledMarathonPlanSection = styled('section')`
    padding: 28px 35px 42px 35px;
    box-sizing: border-box;
    background: #cbcbcb;
    .section-title {
        margin-bottom: 40px;
        font-family: 'Bodoni-400';
        font-size: 36px;
        line-height: 100%;
        color: #454545;
    }
    .item {
        display: flex;
        margin-bottom: 42px;
        &:last-child {
            margin-bottom: 0;
        }
        &__text {
            font-size: 20px;
            line-height: 120%;
            color: #454545;
            white-space: pre-wrap;
        }
        &__svg-wrap {
            margin-right: 44px;
        }
    }
    @media (min-width: 1024px) {
        padding: 42px 73px 21px 63px;
        .section-title {
            margin-bottom: 50px;
            font-size: 46px;
            text-align: center;
        }
        .steps-wrap {
            display: flex;
            justify-content: space-between;
        }
        .item {
            flex-direction: column;
            width: 203px;
            &__svg-wrap {
                margin-right: 0;
                margin-bottom: 18px;
            }
        }
    }
    @media (min-width: 1280px) {
        padding: 53px 152px 21px 131px;
        .section-title {
            margin-bottom: 61px;
        }
        .item__svg-wrap {
            margin-bottom: 25px;
        }
        .item {
            width: 232px;
        }
    }
    @media (min-width: 1920px) {
        padding: 65px 394px 63px 309px;
        .section-title {
            margin-bottom: 55px;
        }
        .item {
            width: 232px;
        }
    }
`;

export const StyledMarathonStepsSection = styled('section')`
    padding: 41px 35px;
    background: #cbcbcb;
    color: #454545;
    box-sizing: border-box;
    .section-title {
        margin-bottom: 17px;
        font-size: 20px;
        line-height: 120%;
        font-family: 'Circe-700';
        text-transform: uppercase;
    }
    .steps {
        margin-bottom: 30px;
        .item {
            display: flex;
            margin-bottom: 15px;
            &__number {
                margin-right: 15px;
                color: #757575;
                font-size: 96px;
                line-height: 100%;
                font-family: 'Bodoni-400';
            }   
            &__text {
                padding: 9px 0 15px 0;
                font-size: 20px;
                line-height: 120%;
                box-sizing: border-box;
                white-space: pre-wrap;
            }
        }
        &-decor {
            display: none;
        }
    }
    .cost-participation {
        margin-bottom: 37px;
        display: flex;
        align-items: center;
        flex-direction: column;
        font-size: 20px;
        font-family: 'Circe-700';
        &__item {
            line-height: 170%;
            &:first-child {
                text-transform: uppercase;
            }
            .sub {
                font-size: 20px;
                text-transform: uppercase;
            }
            .sup {
                font-size: 32px;
            }
            .divider {
                font-size: 32px;
            }
        }
    }
    .terms-agreement {
        font-size: 16px;
        line-height: 120%;
        text-align: center;
        a {
            color: #454545;
        }
    }
    .pay {
        padding: 18px 58px 14px 68px;
        margin-bottom: 28px;
        left: 50%;
        transform: translate(-50%, 0);
    }
    @media (min-width: 1024px) {
        padding: 50px 63px 35px 63px;
        .section-title {
            margin-bottom: 30px;
        }
        .steps {
            display: flex;
            justify-content: space-between;
            margin-bottom: 11px;
            .item {
                margin-left: -10px;
                width: 100%;
                &__text {
                    padding: 10px 0 10px 12px;
                    max-width: 140px;
                    box-sizing: border-box;
                }
                &__number {
                    margin-right: 0;
                }
                &:nth-child(1) {
                    .item__text {
                        max-width: 155px;
                        padding: 10px 0 10px 12px
                    }
                }
                &:nth-child(2) {
                    .item__text {
                        max-width: 175px;
                        padding: 10px 28px 10px 12px
                    }
                }
            }
        }
        .cost-participation {
            flex-direction: row;
            justify-content: center;
            margin-bottom: 24px;
            &__item {
                &:first-child {
                    &::after {
                        content: '\\00a0\\00a0';
                    }
                }
                .sup, .sub {
                    display: inline-block;
                    transform: translate(0, -2px);
                }
                .divider {
                    padding: 0 7px;
                }
            }
        }
        .pay {
            padding: 18px 38px 14px 50px;
            margin-bottom: 27px;
            &:hover {
                color: #cbcbcb;
                background: #454545;
            }
        }
        .terms-agreement {
            width: 76%;
            margin: 0 auto;
        }
    }
    @media (min-width: 1280px) {
        padding: 54px 129px 39px 129px;
        .section-title {
            font-size: 24px;
        }
        .steps {
            margin: 13px 0 50px 13px;
            .item {
                position: relative;
                box-sizing: border-box;
                &__number {
                    position: relative;
                    &::before {
                        content: '';
                        position: absolute;
                        bottom: -22px;
                        left: 50%;
                        transform: translate(-50%, 0);
                        width: 8px;
                        height: 8px;
                        border: 1px solid #454545;
                        box-sizing: border-box;
                        border-radius: 100%;
                    }
                }
                &__text {
                    padding: 10px 25px 10px 12px;
                    max-width: 220px;
                }
                &::before {
                    content: '';
                    position: absolute;
                    bottom: -19px;
                    left: 30px;
                    width: calc(100% - 18px);
                    height: 1px;
                    background-repeat: repeat-x;
                    background-image: url('/assets/dash.svg');
                }
                &:nth-child(2) {
                    .item__text {
                        max-width: 220px;
                    }
                }
                &:nth-child(3) {
                    .item__text {
                        max-width: 231px;
                    }
                    &::before {
                        width: calc(100% - 12px);
                    }
                }
                &:first-child {
                    &::before {
                        left: 29px;
                    }
                }
                &:last-child {
                    &::before {display: none;}
                    .item__number {
                        position: relative;
                        &::before {
                            left: calc(50% + 7px);
                        }
                    }
                }
            }
            &-decor {
                display: flex;
            }
        }
        .pay {
            margin-bottom: 32px;
        }
    }
    @media (min-width: 1920px) {
        padding: 54px 309px 36px 309px;
        .section-title {
            margin-bottom: 32px;
            font-size: 20px;
        }
        .steps {
            margin: 0 0 49px 4px;
        }
        .cost-participation {
            margin-bottom: 29px;
        }
        .pay {
            padding: 18px 45px 14px 45px;
            margin-bottom: 28px;
        }
        .terms-agreement {
            width: 68%;
        }
    }
`;

export const StyledOfferPersonalAccountSection = styled('section')`
    padding: 0 0 56px 0;
    background: #e5e5e5;
    color: #454545;
    box-sizing: border-box;
    .offer-bg {
        
    }
    .content {
        padding: 35px 35px 0 35px;
        box-sizing: border-box;
        &__title {
            margin-bottom: 25px;
            font-size: 20px;
            line-height: 120%;
            text-transform: uppercase;
        }
        &__subtitle {
            margin-bottom: 20px;
            font-size: 20px;
            line-height: 120%;
        }
    }
    .notice {
        margin-bottom: 25px;
        font-family: 'Circe-700';
        font-size: 20px;
        line-height: 120%;
        text-transform: uppercase;
    }
    .features {
        font-size: 20px;
        
        strong {
            font-family: 'Circe-700';
        }
        
        ul {
            li {
                position: relative;
                padding-left: 37px;
                margin: 14px 0;
                &::before {
                    content: '';
                    position: absolute;
                    display: block;
                    top: 7px;
                    left: 0;
                    width: 8px;
                    height: 8px;
                    border: 1px solid #454545;
                    box-sizing: border-box;
                    border-radius: 50%;
                }
            }
        }
    }
    @media (min-width: 1024px) {
        display: flex;
        padding: 0;
        .offer-bg {
            position: relative;
            overflow: hidden;
            flex: 1.19 0 0;
            img {
                position: absolute;
                width: 100%;
                height: 100%;
                object-fit: cover;
            }
        }
        .features {
            &__item {
                padding-left: 32px;
            }
        }
        .content {
            padding: 70px 35px 63px 50px;
            flex: 1 0 0;
            &__subtitle {
                margin-bottom: 16px;
            }
        }
        .notice {
            margin-bottom: 40px;
        }
    }
    @media (min-width: 1280px) {
        .offer-bg {
            width: 50%;
            flex: initial;
        }
        .content {
            width: 50%;
            padding: 70px 35px 63px 106px;
            flex: initial;
        }
    }
    @media (min-width: 1920px) {
        .notice {
            margin-bottom: 37px;
        }
        .content {
            &__title {
                margin-bottom: 28px;
            }
            &__subtitle {
                width: 57%;
            }
        }
    }
`;

export const StyledPreFooterSection = styled('section')`
    display: grid;
    grid-template-columns: auto;
    grid-template-rows: auto;
    grid-template-areas: 
    "section-title" 
    "section-subtitle"
    "cost-participation" 
    "pay" 
    "terms-agreement" 
    "food-img-wrap"
    "get-in-touch";
    padding: 46px 0 60px 0;
    background-color: #e5e5e5;
    background-image: ${({backgroundImage}) => backgroundImage ? `url("${backgroundImage}")` : 'none'};
    background-repeat: no-repeat;
    background-position-x: left;
    background-position-y: top;
    background-size: auto 54%;
    color: #454545;
    .section {
        &-title {
            grid-area: section-title;
            padding: 0 44px 0 41px;
            margin-bottom: 20px;
            font-size: 36px;
            line-height: 100%;
            font-family: 'Bodoni-400';
            box-sizing: border-box;
            white-space: pre-wrap;
        }
        &-subtitle {
            grid-area: section-subtitle;
            padding: 0 44px 0 41px;
            margin-bottom: 12px;
            text-transform: uppercase;
            font-size: 20px;
            line-height: 172%;
            box-sizing: border-box;
        }
    }
    .author-photo-img-wrap {
        grid-area: author-photo-img-wrap;
        display: none;
    }
    .get-in-touch {
        grid-area: get-in-touch;
        padding: 0 20px 0 41px;
        margin: 44px 0 0 0;
        box-sizing: border-box;
        &__title {
            margin-bottom: 30px;
            font-size: 36px;
            line-height: 100%;
            font-family: 'Bodoni-400';
        }
        &__subtitle {
            margin-bottom: 40px;
            font-size: 20px;
            line-height: 120%;
        }
        &__link {
            padding: 18px 53px 14px 64px;
            justify-self: start;
        }
    }
    .cost-participation {
        grid-area: cost-participation;
        padding: 0 44px 0 41px;
        margin-bottom: 24px;
        display: flex;
        align-items: flex-start;
        flex-direction: column;
        font-size: 20px;
        font-family: 'Circe-700';
        &__item {
            line-height: 170%;
            &:first-child {
                text-transform: uppercase;
            }
            .sub {
                font-size: 20px;
                text-transform: uppercase;
            }
            .sup {
                font-size: 32px;
            }
            .divider {
                font-size: 32px;
            }
        }
    }
    .pay {
        padding: 18px 54px 14px 57px;
        grid-area: pay;
        margin: 0 30px 30px 37px;
        justify-self: start;
    }
    .terms-agreement {
        margin-bottom: 12px;
        padding: 0 44px 0 41px;
        font-size: 16px;
        line-height: 120%;
        white-space: pre-wrap;
        a {
            color: #454545;
        }
    }
    .food-img-wrap {
        grid-area: food-img-wrap;
    }
    @media (min-width: 1024px) {
        padding: 46px 64px 60px 64px;
        display: grid;
        grid-template-columns: 1fr 1fr;
        grid-template-rows: repeat(3, auto);
        grid-template-areas: 
        "section-title author-photo-img-wrap" 
        "section-subtitle author-photo-img-wrap" 
        "cost-participation author-photo-img-wrap" 
        "pay author-photo-img-wrap" 
        "terms-agreement get-in-touch" 
        "food-img-wrap get-in-touch";
        background-size: cover;
        box-sizing: border-box;
        .section {
            &-title {
                padding: 0 25px 0 0;
                margin-bottom: 26px;
                font-size: 46px;
            }
            &-subtitle {
                padding: 0 44px 0 0;
                margin-bottom: 20px;
            }
        }
        .author-photo-img-wrap {
            display: block;
        }
        .cost-participation {
            padding: 0 44px 0 0;
            margin-bottom: 30px;
            &__item {
                line-height: 180%;
            }
        }
        .pay {
            margin: 0;
            padding: 18px 44px 14px 47px;
            align-self: flex-start;
            &:hover {
                cursor: pointer;
                background: #454545;
                color: #cbcbcb;
            }
        }
        .terms-agreement {
            padding: 0 44px 0 0;
            margin-top: 27px;
            margin-bottom: 31px;
        }
        .food-img-wrap {
            padding: 0 40px 0 0;
        }
        .get-in-touch {
            margin: 0;
            padding: 0;
            display: flex;
            flex-direction: column;
            &__title {
                margin: 20px 0 30px;
                font-size: 46px;
            }
            &__link {
                margin-top:auto;
                padding: 18px 43px 14px 54px;
                align-self: flex-start;
                &:hover {
                    cursor: pointer;
                    background: #454545;
                    color: #cbcbcb;
                }
            }
        }
    }
    @media (min-width: 1280px) {
        padding: 77px 131px 93px 131px;
        .section-title {
            margin-bottom: 5px;
        }
        .section-subtitle {
            margin-bottom: -4px;
        }
        .cost-participation {
            flex-direction: row;
            align-items: baseline;
            margin-bottom: 7px;
            flex-wrap: wrap;
            &__item {
                margin-right: 7px;   
            }
        }
        .terms-agreement {
            margin-top: 6px;
            margin-bottom: 38px;
            padding: 0 139px 0 0;
        }
        .food-img-wrap {
            align-self: end;
            padding: 0 135px 0 0;
        }
        .get-in-touch {
            &__link {
                padding: 18px 44px 14px 44px;
            }
            &__title {
                margin: 58px 0 66px;
            }
            &__subtitle {
                margin-bottom: 83px;
            }
        }
    }
    @media (min-width: 1920px) {
        padding: 83px 309px 86px 309px;
        grid-template-rows: auto;
        grid-template-areas:
        "section-title author-photo-img-wrap"
        "section-subtitle author-photo-img-wrap"
        "cost-participation author-photo-img-wrap"
        "pay author-photo-img-wrap"
        "terms-agreement author-photo-img-wrap"
        "food-img-wrap get-in-touch";
        .section-title {
            padding: 0 127px 0 0;
            margin-bottom: 33px;
        }
        .section-subtitle {
            margin-bottom: 24px;
        }
        .get-in-touch {
            &__title {
                margin: 70px 0 54px;
            }
            &__subtitle {
                margin-bottom: 67px;
            }
            &__link {
                padding: 18px 35px 14px 44px;
            }
        }
        .cost-participation {
            margin-bottom: 48px;
        }
        .terms-agreement {
            padding: 0 154px 0 0;
            margin-bottom: 0;
        }
        .food-img-wrap {
            padding: 0 159px 0 0;
        }
        .pay {
            padding: 18px 45px 14px 45px;
            margin: 0 0 26px 0;
        }
    }
`;