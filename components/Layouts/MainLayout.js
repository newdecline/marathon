// import React, {useEffect} from "react";
// import PropTypes from "prop-types";
// import {Header} from "../Header/Header";
// import {Footer} from "../Footer/Footer";
// import Head from "next/head";
// import parse from 'html-react-parser';
// import {useRouter} from "next/router";
// import {inject, observer} from "mobx-react";
//
// export const MainLayout = inject('fetchDataStore')(observer(props => {
//     const {
//         children,
//         fetchDataStore: {data}
//     } = props;
//
//     const router = useRouter();
//
//     const handleRouteChange = url => {
//         if (history.state.as !== url) {
//             typeof Ya === 'function' && Ya.Metrika2.counters().forEach(({id}) => {
//                 ym(id, 'hit', url);
//             });
//
//             typeof ga === 'function' && ga.getAll().forEach(item => {
//                 gtag('config', item.b.data.values[':trackingId'], {page_path: url});
//             });
//         }
//     };
//
//     useEffect(() => {
//         router.events.on('beforeHistoryChange', handleRouteChange);
//
//         return () => {
//             router.events.off('beforeHistoryChange', handleRouteChange);
//         }
//     }, []);
//
//     return (
//         <>
//             <Head>
//                 <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
//                 <link rel="icon" type="image/x-icon" href="/assets/favicon.ico"/>
//                 <title>Default title</title>
//                 {data.customCode && parse(`${data.customCode.head}`)}
//             </Head>
//
//             <Header/>
//             {children}
//             <Footer/>
//         </>
//     )
// }));
//
// MainLayout.propTypes = {
//     children: PropTypes.object
// };