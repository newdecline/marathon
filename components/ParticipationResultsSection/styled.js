import styled from "styled-components";

export const Styled = styled('section')`
    padding: 35px 0 49px 35px;
    box-sizing: border-box;
    background: #e5e5e5;
    position: relative;
    overflow: hidden;
    .swiper-wrap {
        transition: transform .3s
    }
    .section-title {
        display: none;
    }
    .slade-wrap {
        padding-top: 5px;
        margin-right: 32px;
        width: 77.4%;
        box-sizing: border-box;
    }
    .slide { 
        position: relative;
        padding: 20px;
        background: #fff;
        box-sizing: border-box;
        &__img-wrap {
            margin-bottom: 16px;
            width: 100%;
            overflow: hidden;
            img {
                width: inherit;
            }
        }
        &__footer {
            display: flex;
            align-items: flex-end;
            color: #757575;
        }
        &__watch-difference-btn {
            display: flex;
            align-items: center;
            position: absolute;
            top: 37px;
            right: -8px;
            padding: 6px;
            background: #757575;
            color: #e5e5e5;
            border-radius: 15px 3px 3px 15px;
            border: none;
            box-sizing: border-box;
            transform: rotate(-90deg);
            font-family: 'Circe-300';
            transition: background-color .3s;
            .text {
                margin-left: 2px;
                margin-right: 7px;
                font-size: 16px;
                line-height: 120%;
            }
            .icon-wrap {
                width: 20px;
                height: 17px;
                transform: rotate(90deg);
                svg {
                    width: 100%;
                    height: 100%;
                }
            }
        }
    }
    .weight-difference {
        margin-right: auto;
        font-size: 36px;
        line-height: 100%;
        font-family: 'Bodoni-400';
    }
    .weight-start {
        position: relative;
        width: 73px;
        &::before {
            position: absolute;
            left: 4px;
            top: 50%;
            content: '';
            display: block;
            width: 100%;
            height: 1px;
            background-color:#757575;
            transform: translate3d(-15px, -50%, 0);
        }
        .icon-wrap {
            position: absolute;
            top: 50%;
            right: -5px;
            display: flex;
            width: 5px;
            height: 8px;
            transform: translate3d(-15px, -50%, 0);
            svg {
                width: 100%;
                height: 100%;
            }
        }
    }
    .weight-start, .weight-end {
        font-size: 20px;
        line-height: 120%;
        transform: translate(0, -2px);
    }
    .swiper-pagination-progressbar {
        display: none;
    }
    .swiper-button-next, .swiper-button-prev {
        display: none;
    }
    .swiper-container {
        overflow: unset
    }
    @media (min-width: 1024px) {
        padding: 51px 0 118px 64px;
        overflow: unset;
        .slade-wrap {
            width: 40.2%;
        }
        .section-title {
            display: block;
            position: absolute;
            top: 155px;
            left: -86px;
            font-size: 20px;
            line-height: 120%;
            text-transform: uppercase;
            transform: rotate(-90deg);
        }
        .slide {
            padding: 30px;
        }
        .slide__watch-difference-btn {
            right: 3px;
            &:hover {
                background: #454545;
                cursor: pointer;
            }
        }
        .weight-start, .weight-end {
        }
        .weight-difference {
            font-size: 40px;
            margin-right: 26px;
        }
        .weight-start {
            width: 76px;
        }
        .swiper-container {
            position: static;
            overflow: hidden;
            .swiper-pagination-progressbar {
                display: block;
                top: unset;
                left: 64px;
                bottom: 50px;
                width: calc(100% - 128px);
                height: 2px;
            }
            .swiper-pagination-progressbar-fill {
                background: #454545;
                border-radius: 4px;
            }
            .swiper-button-next, .swiper-button-prev {
                display: block;
                top: auto;
                bottom: 29px;
                background: transparent url(/assets/arrow.svg) no-repeat center;
                &::after {
                    display: none;
                }
            }
            .swiper-button-next {
                right: 37px;
            }
            .swiper-button-prev {
                left: 36px;
                transform: rotate(180deg);
            }
        }
    }
    @media (min-width: 1280px) {
        padding: 81px 0 107px 133px;
        .section-title {
            top: 194px;
            left: -34px;
        }
        .slade-wrap {
            width: 30.5%;
            margin-right: 64px; 
        }
        .swiper-container {
            .swiper-pagination-progressbar {
                bottom: 53px;
                left: 131px;
                width: calc(100% - 260px);
            }
            .swiper-button-next,
            .swiper-button-prev {
                bottom: 32px;
            }
            .swiper-button-next {
                right: 105px;
            }
            .swiper-button-prev {
                left: 105px;
            }
        }
    }
    @media (min-width: 1920px) {
        padding: 83px 0 102px 309px;
        .slade-wrap {
            width: 23.55%;
            margin-right: 60px; 
        }
        .section-title {
            left: -52px;
        }
        .swiper-container {
            .swiper-pagination-progressbar {
                bottom: 53px;
                left: 310px;
                width: calc(100% - 621px);
            }
            .swiper-button-next {
                right: 281px;
            }
            .swiper-button-prev {
                left: 280px;
            }
        }
    }
`;