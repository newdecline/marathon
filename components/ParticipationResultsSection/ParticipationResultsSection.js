import React, {useState, useEffect, useRef} from 'react';
import Swiper from 'react-id-swiper';
import EyeIcon from '../../svg/eye.svg';
import ArrowIcon from '../../svg/arrow.svg';
import {Styled} from "./styled";
import {inject, observer} from "mobx-react";
import {PhotosResultsPopup} from "../PhotosResultsPopup/PhotosResultsPopup";
import {Portal} from 'react-portal';
import {disablePageScroll, enablePageScroll} from "scroll-lock";
import {normalizeDataForTheGalleryOfResults} from "../../utilities/normalizeDataForTheGalleryOfResults";

export const ParticipationResultsSection = inject('appStore', 'fetchDataStore')(observer(props => {
  const {fetchDataStore: {data: {results}}} = props;

  const photosResultsPopup = useRef(null);

  const [swiper, setSwiper] = useState(null);
  const [startIndex, setStartIndex] = useState(0);
  const [isShowBeforeAfterPhotos, setIsShowBeforeAfterPhotos] = useState(false);
  useEffect(() => {
    if (isShowBeforeAfterPhotos) {
      disablePageScroll(photosResultsPopup.current);
    } else {
      enablePageScroll(photosResultsPopup.current);
    }
  }, [isShowBeforeAfterPhotos]);

  useEffect(() => {
    if (swiper !== null) {

    }
  }, [swiper]);

  const sliderOptions = {
    slidesPerView: 1.26,
    slidesOffsetAfter: 32,
    spaceBetween: 32,
    breakpoints: {
      1024: {
        slidesPerView: 2.87,
        spaceBetween: 47,
        height: 446,
        slidesOffsetAfter: 63
      },
      1280: {
        slidesPerView: 2.93,
        spaceBetween: 64,
        slidesOffsetAfter: 130
      },
      1920: {
        slidesPerView: 3.8,
        spaceBetween: 60,
        slidesOffsetAfter: 310
      }
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'progressbar',
    },
    getSwiper: (swiper) => {
      setSwiper(swiper)
    }
  };

  const getAllPhotosParticipation = () => normalizeDataForTheGalleryOfResults(results).map(item => item.photosWithResults);

  const onShowBeforeAfterPhotos = photoIndex => {
    setIsShowBeforeAfterPhotos(!isShowBeforeAfterPhotos);
    setStartIndex(photoIndex);
  };

  const onCloseBeforeAfterPhotos = () => {
    setIsShowBeforeAfterPhotos(!isShowBeforeAfterPhotos);
  };

  return (
    <Styled id='participation-results'>
      {isShowBeforeAfterPhotos
      && <Portal>
        <PhotosResultsPopup
          ref={photosResultsPopup}
          onCloseBeforeAfterPhotos={onCloseBeforeAfterPhotos}
          isShowBeforeAfterPhotos={isShowBeforeAfterPhotos}
          allPhotos={getAllPhotosParticipation()}
          allParticipation={results}
          initialIndex={startIndex}/>
      </Portal>}
      <div className="section-title">Результаты участниц</div>
      <Swiper {...sliderOptions}>
        {
          normalizeDataForTheGalleryOfResults(results).map((item, idx) => {
            return (
              <div className="slade-wrap" key={idx}>
                <div className='slide'>
                  <div className="slide__img-wrap"><img src={item.avatar} alt="alt"/></div>
                  <div className="slide__footer">
                    <div className="weight-difference">-{item.weightDifference}кг</div>
                    <div className="weight-start">
                      <div className="icon-wrap"><ArrowIcon/></div>
                      {item.weightStart} кг
                    </div>
                    <div className="weight-end">{item.weightEnd} кг</div>
                  </div>

                  <button onClick={() => {
                    onShowBeforeAfterPhotos(idx)
                  }}
                          type='button'
                          className="slide__watch-difference-btn">
                    <span className="icon-wrap"><EyeIcon/></span>
                    <span className="text">до - после</span>
                  </button>
                </div>
              </div>
            )
          })
        }
      </Swiper>

    </Styled>
  )
}));