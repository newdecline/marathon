import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {StyledLabelInput, StyledLoginForm, StyledRememberMe} from "./styled";
import {useForm} from 'react-hook-form';
import CrossFormIcon from '../../svg/cross-form.svg';
import {inject, observer} from "mobx-react";
import {CSSTransition} from "react-transition-group";
import {disablePageScroll, enablePageScroll} from 'scroll-lock';
import {ButtonWithLinearProgress} from "./ButtonWithLinearProgress/ButtonWithLinearProgress";

export const LoginForm = inject('appStore', 'fetchDataStore')(observer(props => {
    const {
        isExpandHeightHeader,
        appStore,
        fetchDataStore: {
            data: {
                layout: {whats_app_url}
            }
        }
    } = props;

    const {register, handleSubmit} = useForm();

    const [loginError, setLoginError] = useState([]);
    const [loading, setLoading] = useState(false);

    const isOpenLoginForm = appStore.isOpenLoginForm;

    useEffect(() => {
        const urlParams = new URLSearchParams(window.location.search);
        const openLoginForm = urlParams.get('open_login_form');

        if (openLoginForm) {
            appStore.setIsOpenLoginForm();
        }
    }, []);

    useEffect(() => {
        if (process.browser) {
            const $scrollableElement = document.querySelector('.inner-wrap');
            if (isOpenLoginForm) {
                disablePageScroll($scrollableElement);
            } else {
                enablePageScroll($scrollableElement);
            }
        }

    }, [isOpenLoginForm]);

    const onSubmit = async data => {
        setLoading(true);
        const response = await fetch('/api/auth/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        });

        if (response.ok) {
            const loginData = await response.json();

            localStorage.setItem('token', loginData.value);
            window.location.href = '/client';
        } else if (response.status === 422) {
            const loginData = await response.json();
            setLoginError(loginData);
        } else {
            // TODO Отобразить текст ошибки из эксепшена.
        }
        setLoading(false);
    };

    const handleClickCloseBtn = () => {
        appStore.setIsOpenLoginForm();
    };

    let errorFields = {};
    loginError.forEach(field => {
        errorFields = {
            ...errorFields,
            [field.field]: field.message
        }
    });
    
    const resetPassword = () => {
        localStorage.setItem('resetPassword', 'true');
        window.location.href = '/client';
    };

    return (
        <StyledLoginForm
            isExpandHeightHeader={isExpandHeightHeader}>
            <CSSTransition
                in={isOpenLoginForm}
                timeout={1000}
                classNames="transition-login-form"
                unmountOnExit>
                <div>
                    <div className="inner-wrap">
                        <button className='close-form' onClick={handleClickCloseBtn}><CrossFormIcon/></button>
                        <div className="inner">

                            <form onSubmit={handleSubmit(onSubmit)} className='form'>
                                <StyledLabelInput>
                                    <input
                                        className={errorFields.email ? "input error" : "input"}
                                        name="email"
                                        ref={register()}/>
                                    <span className="placeholder">E-mail</span>
                                </StyledLabelInput>

                                <StyledLabelInput>
                                    <input
                                        className={errorFields.password ? "input error" : "input"}
                                        name="password"
                                        type="password"
                                        ref={register()}/>
                                    <span className="placeholder">Пароль</span>
                                </StyledLabelInput>

                                <ButtonWithLinearProgress
                                    bg='#454545'
                                    activeBg='#000'
                                    loading={loading}
                                    text='Войти'
                                    type="submit"
                                    className='submit-button'/>

                                <div className="error-message-container">
                                    {loginError.map((item, i) => {
                                        if (loginError.length - 1 === i) {
                                            return <div
                                                key={item.field}
                                                className='error-message-container__item'>{item.message}</div>
                                        }
                                    })}
                                </div>

                                <StyledRememberMe>
                                    <label className="label">
                                        <input
                                            ref={register()}
                                            type="checkbox"
                                            className='label__input'
                                            name='rememberMe'
                                            defaultChecked
                                        />
                                        <span className='label__quare'/>
                                        <span className='label__text'>Запомнить меня</span>
                                    </label>
                                    <span 
                                        className="forgot-password-link"
                                        onClick={resetPassword}
                                        title="Восстановить пароль"
                                    >
                                        Забыли пароль?
                                    </span>
                                </StyledRememberMe>

                                <p className="notice">
                                    Перед стартом марафона вам на e-mail приходят доступы к личному кабинету.
                                    Если вы не получили письмо или не оплатили участие - свяжитесь с менеджером.
                                </p>

                                <a href={whats_app_url} target='_blank' rel='noopener noreferrer'
                                   className='write-button'>Написать</a>
                            </form>
                        </div>
                    </div>
                </div>
            </CSSTransition>

        </StyledLoginForm>
    )
}));

LoginForm.propTypes = {
    isExpandHeightHeader: PropTypes.bool
};