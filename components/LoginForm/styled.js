import styled from "styled-components";

export const StyledLoginForm = styled('div')`
    position: fixed;
    top: ${({isExpandHeightHeader}) => isExpandHeightHeader ? '70px' : '100px'};
    left: 0;
    width: 100%;
    background-color: ${({isOpen}) => isOpen ? 'rgba(0, 0, 0, 0.6)' : 'rgba(0, 0, 0, 0)'};
    display: block;
    transition: height .3s, top .3s, background-color .3s;
    overflow: hidden;
    z-index: 1;
    .transition-login-form {
        &-enter {
            background-color: rgba(0, 0, 0, 0);
            .inner-wrap {
                transform: translateX(100%);
            }
        }
        &-enter-active {
            background-color: rgba(0, 0, 0, .6);
            transition: background-color .3s;
            .inner-wrap {
                transform: translateX(0);
                transition: transform .3s;
            }
        }
        &-exit {
            background-color: rgba(0, 0, 0, .6);
            transition: background-color .3s;
            .inner-wrap {
                transform: translateX(0);
            }  
        }
        &-exit-active {
            background-color: rgba(0, 0, 0, 0);
            transition: background-color .3s;
            .inner-wrap {
                transform: translateX(100%);
                transition: transform .3s;
            }
        }
        &-enter-done {
            background-color: rgba(0, 0, 0, .6);
        }
    }
    .inner-wrap {
        position: relative;
        padding: 96px 25px 58px 27px;
        height: ${({isExpandHeightHeader}) => isExpandHeightHeader
        ? 'calc((var(--vh, 1vh) * 100) - 70px)'
        : 'calc((var(--vh, 1vh) * 100) - 100px)'};
        background: #cbcbcb;
        color: #454545;
        overflow: auto;
        box-sizing: border-box; 
    }
    .inner {
        display: flex;
        width: 100%;
        
    }
    .form {
        display: grid;
    }
    .close-form {
        position: absolute;
        right: 25px;
        top: 37px;
        display: flex;
        border: none;
        background-color: transparent;
        padding: 0;
        margin: 0;
    }
    .notice {
        margin-top: auto;
        margin-bottom: 28px;
        font-size: 20px;
        line-height: 120%;
        font-weight: 600;
    }
    .write-button {
        display: block;
        padding: 21px 0 15px 0;
        //margin: 0 0 58px 0;
        text-decoration: none;
        text-align: center;
        border: 2px solid #454545;
        background: #cbcbcb;
        box-sizing: border-box;
        color: #454545;
        font-size: 20px;
        line-height: 20px;
        font-family: 'Circe-700';
        text-transform: uppercase;
        letter-spacing: 0.4em;
        transition: border .3s, color .3s, background-color .3s;
    }
    .submit-button {
        min-height: 60px;
        display: block;
        margin-bottom: 30px;
        border: 0;
        background: #454545;
        color: #fff;
        text-align: center;
        font-family: 'Circe-700';
        letter-spacing: 0.4em;
        font-size: 20px;
        box-sizing: border-box;
        text-transform: uppercase;
        transition: border .3s, color .3s, background-color .3s;
    }
    .error-message-container {
        margin-bottom: 30px;
        font-size: 20px;
        line-height: 120%;
        color: #D96868;
        &__item {
            
        }
    }
    @media (min-width: 1024px) {
        .inner-wrap {
            width: 507px;
            margin-left: auto;
        }
        .form {
            grid-template-rows: auto;
        }
        .close-form {
            &:hover {
                cursor: pointer;
            }
        }
        .write-button {
            margin-bottom: 58px;
            align-self: flex-end;
            &:hover {
                background-color: #000;
                color: #fff;
            }
        }
        .submit-button {
            align-self: flex-start;
            &:hover {
                cursor: pointer;
                background-color: #000;
            }
        }
    }
`;

export const StyledLabelInput = styled('label')`
    position: relative;
    display: block;
    width: 100%;
    margin-bottom: 32px;
    .input {
        padding: 16px 14px 16px 19px;
        display: block;
        width: 100%;
        border: 2px solid #454545;
        color: #454545;
        background-color: transparent;
        box-sizing: border-box;
        font-size: 18px;
        font-family: 'Circe-300';
        font-weight: 600;
        transition: border .3s;
        outline: none;
        &.error {
            border: 2px solid #D96868;
            & + .placeholder {
                color: #D96868;
            }
        }
    }
    .placeholder {
        position: absolute;
        top: -10px;
        left: 14px;
        display: inline-block;
        padding: 0 5px;
        background: #cbcbcb;
        font-size: 18px;
        line-height: 120%;
        font-weight: 600;
        transition: color .3s;
    }
`;

export const StyledRememberMe = styled('div')`
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 63px;
    .label {
        position: relative;
        padding-left: 28px;
        display: flex;
        align-items: center;
        &__input {
            position: absolute;
            z-index: -1;
            visibility: hidden;
            &:checked + .label__quare {
                background-color: #454545;
            }
        }
        &__quare {
            position: absolute;
            top: 1px;
            left: 0;
            width: 15px;
            height: 15px;
            border: 1px solid #454545;
            border-radius: 3px;
            box-sizing: border-box;
            transition: background-color .3s;
        }
        &__text {
            font-size: 18px;
            line-height: 120%;
            font-weight: 600;
        }
    }
    .forgot-password-link {
        font-size: 18px;
        line-height: 120%;
        color: #454545;
        font-weight: 600;
        cursor: pointer;
        text-decoration: underline;
    }
`;

