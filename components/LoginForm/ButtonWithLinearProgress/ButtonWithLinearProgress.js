import React from "react";
import PropTypes from 'prop-types';
import {StyledButtonWithLinearProgress} from "./styled";

export const ButtonWithLinearProgress = props => {
    const {
        bg,
        activeBg,
        loading,
        text,
        className,
        disabled,
        type} = props;

    return (
        <StyledButtonWithLinearProgress
            bg={bg}
            activeBg={activeBg}
            disabled={disabled}
            type={type}
            className={className}>
            <span className="text">{text}</span>
            {loading && <span className="progress-container">
                    <span className="progress-container__active"/>
            </span>}
        </StyledButtonWithLinearProgress>
    )
};

ButtonWithLinearProgress.propTypes = {
    text: PropTypes.string,
    className: PropTypes.string,
    disabled: PropTypes.any,
    type: PropTypes.string,
    loading: PropTypes.bool,
    bg: PropTypes.string,
    activeBg: PropTypes.string
};