import styled, {keyframes} from 'styled-components';

const translation = keyframes`
    0% {
        left: -100%;
        transform: scale(1, 1);
    }
    
    90% {
        transform: scale(7, 1);
    }
    
    100% {
        left: 200%;
        transform: scale(1, 1);
    }
`;

export const StyledButtonWithLinearProgress = styled('button')`
    overflow: hidden;
    position:relative;
    outline: none;
    .text {
        position:relative;
        z-index: 1;
    }
    .progress-container {
        position:absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        display: block;
        background: ${({bg}) => bg};
        &__active {
            position:absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 20%;
            display: block;
            background: ${({activeBg}) => activeBg};
            animation: ${translation} 1s ease-in infinite;
        }
    }
`;