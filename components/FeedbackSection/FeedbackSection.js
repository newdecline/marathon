import React, {useState, useEffect, useRef} from 'react';
import Swiper from 'react-id-swiper';
import PhotoCameraIcon from '../../svg/photo-camera.svg';
import {Styled} from "./styled";
import {inject, observer} from "mobx-react";
import {disablePageScroll, enablePageScroll} from "scroll-lock";
import {Portal} from "react-portal";
import {PhotosFeedbackPopup} from "../PhotosFeedbackPopup/PhotosFeedbackPopup";
import {normalizeDataForTheGalleryOfFeedbacks} from "../../utilities/normalizeDataForTheGalleryOfFeedbacks";

export const FeedbackSection = inject('appStore', 'fetchDataStore')(observer(props => {
  const {fetchDataStore: {data: {reviews}}} = props;

  const photosFeedbackPopup = useRef(null);

  const [startIndex, setStartIndex] = useState(0);
  const [isShowFeedbackPhotos, setIsShowFeedbackPhotos] = useState(false);
  useEffect(() => {
    if (isShowFeedbackPhotos) {
      disablePageScroll(photosFeedbackPopup.current);
    } else {
      enablePageScroll(photosFeedbackPopup.current);
    }
  }, [isShowFeedbackPhotos]);

  const sliderOptions = {
    slidesPerView: 1.26,
    spaceBetween: 32,
    slidesOffsetAfter: 32,
    breakpoints: {
      1024: {
        slidesPerView: 1
      },
      1280: {
        spaceBetween: 50,
        slidesPerView: 2
      }
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'progressbar',
    },
  };

  const getAllPhotosParticipation = () => normalizeDataForTheGalleryOfFeedbacks(reviews).map(item => item.imgSrc);

  const onShowFeedbackPhotos = photoIndex => {
    setIsShowFeedbackPhotos(!isShowFeedbackPhotos);
    setStartIndex(photoIndex);
  };

  const onCloseFeedbackPhotos = () => {
    setIsShowFeedbackPhotos(!isShowFeedbackPhotos);
  };

  const normalizeTextReviews = (text) => {
    if (text.length > 202) {
      return `${text.slice(0, 199)}...`
    }
    return text
  };

  return (
    <Styled id='feedback'>
      {isShowFeedbackPhotos
      && <Portal>
        <PhotosFeedbackPopup
          ref={photosFeedbackPopup}
          onCloseFeedbackPhotos={onCloseFeedbackPhotos}
          isShowFeedbackPhotos={isShowFeedbackPhotos}
          allPhotos={getAllPhotosParticipation()}
          initialIndex={startIndex}/>
      </Portal>}

      <div className="section-title">Отзывы</div>

      <Swiper {...sliderOptions}>
        {
          normalizeDataForTheGalleryOfFeedbacks(reviews).map((item, idx) => {
            return (
              <div className="slade-wrap" key={idx}>
                <div className='slide' >
                  <div className="slide__img-wrap"><img src={item.avatar} alt="alt" /></div>

                  <div className="slide__name">{item.name}</div>

                  <div className="slide__text">{normalizeTextReviews(item.text)}</div>

                  <button onClick={() => {onShowFeedbackPhotos(idx)}} type='button' className="slide__photo-btn">
                    <span className="icon-wrap"><PhotoCameraIcon /></span>
                  </button>
                </div>
              </div>
            )
          })
        }
      </Swiper>
    </Styled>
  )
}));