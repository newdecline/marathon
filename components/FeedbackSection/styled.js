import styled from "styled-components";

export const Styled = styled('section')`
    padding: 34px 0 58px 25px;
    box-sizing: border-box;
    background: #cbcbcb;
    overflow: hidden;
    .section-title {
        display: none;
    }
    .slade-wrap {
        padding-top: 17px;
        padding-left: 17px;
        width: 77.6%;
        margin-right: 32px;
        box-sizing: border-box;
    }
    .slide { 
        height: 478px;
        position: relative;
        padding: 89px 20px 50px 20px;
        background: #e5e5e5;
        box-sizing: border-box;
        color: #454545;
        &__img-wrap {
          position: absolute;
          top: -17px;
          left: -17px;
          width: 90px;
          height: 90px;
          overflow: hidden;
          img {
              width: inherit;
          }
        }
        &__name {
            margin-bottom: 16px;
            font-size: 36px;
            line-height: 100%;
            font-family: 'Bodoni-400';
        }
        &__text {
            margin-bottom: 20px;
            font-size: 20px;
            line-height: 120%;
            height: 216px;
            overflow: hidden;
            white-space: pre-wrap;
        }
        &__photo-btn {
            position: absolute;
            right: 0;
            bottom: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            width: 52px;
            height: 52px;
            padding: 0;
            border: none;
            background: #757575;
            transition: background .3s;
            .icon-wrap {
                display: flex;
                width: 26px;
                height: 21px;
                svg {
                    width: 100%;
                    height: 100%;
                }
            }
        }
    }
    .swiper-container {
        overflow: unset;
    }
    .swiper-pagination-progressbar, .swiper-button-next, .swiper-button-prev {
        display: none;
    }
    @media (min-width: 1024px) {
        padding: 66px 63px 97px 64px;
        position: relative;
        overflow: unset;
        .section-title {
            display: block;
            position: absolute;
            top: 97px;
            left: -8px;
            font-size: 20px;
            line-height: 120%;
            text-transform: uppercase;
            transform: rotate(-90deg);
        }
        .slade-wrap {
            padding-top: 27px;
            width: 100%;
        }
        .slide {
            height: 271px;
            padding: 0 20px 50px 20px;
            &__img-wrap {
                top: -26px;
                width: 157px;
                height: 157px;
            }
            &__name {
                margin-bottom: 23px;
                padding: 37px 0 0 162px;
                font-size: 46px;
            }
            &__text {
                padding: 0 0 0 162px;
                height: 144px;
            }
            &__photo-btn {
                &:hover {
                    cursor: pointer;
                    background: #454545;
                }
            }
        }
        .swiper-container {
            position: static;
            overflow: hidden;
            .swiper-pagination-progressbar {
                display: block;
                top: unset;
                left: 64px;
                bottom: 51px;
                width: calc(100% - 128px);
                height: 2px;
            }
            .swiper-pagination-progressbar-fill {
                background: #454545;
                border-radius: 4px;
            }
            .swiper-button-next, .swiper-button-prev {
                display: block;
                top: auto;
                bottom: 30px;
                background: transparent url(/assets/arrow.svg) no-repeat center;
                &::after {
                    display: none;
                }
            }
            .swiper-button-next {
                right: 37px;
            }
            .swiper-button-prev {
                left: 36px;
                transform: rotate(180deg);
            }
        }
    }
    @media (min-width: 1280px) {
        padding: 58px 129px 107px 133px;
        .slade-wrap {
            width: 47.6%;
            margin-right: 50px;
        }
        .section-title {
            top: 89px;
            left: 45px;
        }
        .swiper-container {
            .swiper-pagination-progressbar {
                bottom: 53px;
                left: 131px;
                width: calc(100% - 260px);
            }
            .swiper-button-next,
            .swiper-button-prev {
                bottom: 32px;
            }
            .swiper-button-next {
                right: 105px;
            }
            .swiper-button-prev {
                left: 105px;
            }
        }
        .slide {
            padding: 0 39px 50px 20px;
            height: 412px;
        }
    }
    @media (min-width: 1920px) {
        padding: 58px 309px 118px 309px;
        .slide {
            height: 340px;
        }
        .slade-wrap {
            width: 48.1%;
        }
        .section-title {
            left: 23px;
            top: 86px;
        }
        .swiper-container {
            .swiper-pagination-progressbar {
                bottom: 55px;
                left: 310px;
                width: calc(100% - 621px);
            }
            .swiper-button-next,
            .swiper-button-prev {
                bottom: 34px;
            }
            .swiper-button-next {
                right: 281px;
            }
            .swiper-button-prev {
                left: 280px;
            }
        }
    }
`;