import React from 'react';
import {observer, inject} from "mobx-react";
import InstagramIcon from "../../svg/instagram.svg";
import WhatsappIcon from "../../svg/whatsapp.svg";
import PinkchilliIcon from "../../svg/pinkchilli-logo.svg";
import {NavigationList} from "../NavigationList/NavigationList";
import {StyledFooter} from "./styled";

export const Footer = inject('fetchDataStore')(observer(props => {
    const {
        fetchDataStore: {
            data: {
                layout: {copyright, instagram_url, whats_app_url}
            }
        }
    } = props;

    // TODO По переходу в "Личный кабинет" открывать форму логина, если нет токена.
    return (
        <StyledFooter>
            <div className="col">
                <div className="copyright">
                    <div className="copyright__text">{copyright}</div>
                    <span className='date'>{new Date().getFullYear()}</span>
                </div>
            </div>

            <div className="col">
                <NavigationList/>
            </div>

            <div className="col">
                <ul className='additional-menu'>
                    <li className='additional-menu__item'>
                        <a href="/client" target='_blank' rel='noopener noreferrer'>Личный кабинет</a>
                    </li>
                    <li className='additional-menu__item'>
                        <a target='_blank' rel='noopener noreferrer' href={whats_app_url}>Оплатить
                            участие</a>
                    </li>

                    <ul className='social-list'>
                        <li className="social-list__item">
                            <a href={instagram_url} target='_blank' rel='noopener noreferrer'
                               className='social-list__link'><InstagramIcon/></a>
                        </li>
                        <li className="social-list__item">
                            <a target='_blank' rel='noopener noreferrer' href={whats_app_url}
                               className='social-list__link'><WhatsappIcon/></a>
                        </li>
                    </ul>
                </ul>
            </div>

            <div className="col">
                <a href='https://pinkchilli.agency' target='_blank' rel='noopener noreferrer' className="developed-by">
                    <div className="developed-by__text">Разработка</div>
                    <div className="developed-by__agency-name"><PinkchilliIcon/></div>
                </a>
            </div>
        </StyledFooter>
    )
}));

