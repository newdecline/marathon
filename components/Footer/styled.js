import styled from "styled-components";

export const StyledFooter = styled('footer')`
    padding: 38px 30px 35px 41px;
    display: flex;
    flex-direction: column;
    background: #454545;
    color: #fff;
    .navigation-list {
        display: none;
    }
    .additional-menu {
        &__item {
            display: none;
        }
    }
    .social-list {
        display: flex;
        align-items: center;
        margin-bottom: 35px;
        &__item {
            display: flex;
            margin-right: 25px;
            &:nth-child(2) {
                width: 27px;
                height: 27px;
                margin-right: 0;
                .social-list__link {
                    width: 27px;
                    height: 27px;
                }
                svg path{
                    fill: #fff;
                }
            }
        }
        &__link {
            display: inline-flex;
            width: 25px;
            height: 25px;
            svg {
                width: 100%;
                height: 100%;
            }
        }
    }
    .copyright {
        margin-bottom: 26px;
        font-size: 20px;
        line-height: 140%;
        &__text {
            white-space: pre-wrap;
        }
    }
    .developed-by {
        text-decoration: none;
        color: #fff;
        &__text {
            margin-bottom: 15px;
            font-size: 20px;
            line-height: 120%;
        }
        &__agency-name {
            width: 105px;
            height: 37px;
            svg {
                width: 100%;
                height: 100%;
            }
        }
    }
    @media (min-width: 1024px) {
        flex-direction: row;
        justify-content: flex-start;
        padding: 51px 30px 35px 63px;
        .navigation-list {
            display: block;
            &__item {
                margin-right: 0;
                text-transform: unset;
                line-height: 138%;
            }
        }
        .additional-menu {
            &__item {
                display: block;
                font-size: 20px;
                line-height: 138%;
                a {
                    color: #fff;
                    text-decoration: none;
                }
            }
        }
        .developed-by {
            display: inline-block;
            margin-left: 89px;
            &__agency-name {
                width: 139px;
                height: 51px;
            }
        }
        .social-list {
            margin-top: 23px;
            margin-bottom: 0;
        }
        .col {
            width: 24.4%;
        }
    }
    @media (min-width: 1280px) {
        padding: 51px 131px 35px 131px;
        .date {
            display: block;
        }
        .col {
            width: 25%;
        }
        .developed-by {
            display: flex;
            flex-direction: column;
            margin-left: 0;
            &__text {
                padding-left: 132px;
            }
            &__agency-name {
                align-self: flex-end;
            }
        }
    }
    @media (min-width: 1920px) {
        padding: 54px 168px 34px 309px;
        .col {
            width: calc(25% - 36px);
            &:last-child {
                display: flex;
                margin-left: auto;
            }
        }
        .developed-by {
            display: inline-flex;
            margin: 0 0 0 auto;
            &__text {
                padding-left: 0;
            }
        }
    }
`;