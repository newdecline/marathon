import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

export const Button = props => {
    const {text} = props;

    return (
        <ButtonStyled {...props}>
            {text}
        </ButtonStyled>
    )
};

const ButtonStyled = styled('button')`
    position: relative;
    display: inline-block;
    padding: 18px 63px 14px 63px;
    color: #454545;
    font-size: 20px;
    text-transform: uppercase;
    line-height: 120%;
    font-family: 'Circe-700';
    letter-spacing: 0.4em;
    text-decoration: none;
    border: 2px solid #454545;
    box-sizing: border-box;
    transition: color .3s, background-color .3s;
    &:active, &:focus {
        outline: none;
    }
`;

Button.propTypes = {
    text: PropTypes.string
};