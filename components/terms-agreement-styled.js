import styled from 'styled-components';

export const StyledTermsAgreementPage = styled('div')`
    padding: 110px 30px 30px 30px;
    .title {
        margin: 20px 0;
        font-size: 22px;
        font-family: 'Circe-700';
        &__count {
            padding-right: 5px;
        }
    }
    .paragraph {
        margin: 10px 0;
        font-size: 18px;
        font-family: 'Circe-300';
        line-height: 20px;
        &__count {
            padding-right: 5px;
        }
    }
    @media (min-width: 1024px) {
        padding: 110px 30px 30px 35px;
    }
    @media (min-width: 1280px) {
        padding: 110px 30px 30px 70px;
        .paragraph {
            max-width: 80%;
        }
    }
`;