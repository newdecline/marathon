import React, {useEffect} from 'react';
import {useRouter} from "next/router";
import PropTypes from 'prop-types';
import Head from "next/head";
import InstagramIcon from "../svg/instagram.svg";
import CalendarIcon from "../svg/calendar.svg";
import WhatsappIcon from "../svg/whatsapp.svg";
import SaladIcon from "../svg/salad.svg";
import WomanIcon from "../svg/woman.svg";
import {ParticipationResultsSection} from "../components/ParticipationResultsSection/ParticipationResultsSection";
import {FeedbackSection} from "../components/FeedbackSection/FeedbackSection";
import {Button} from "../components/UI/Button";
import DariyaGolubkovaSvgFont from "../svg/dariya-golubkova.svg";
import {
    StyledBannerSection, StyledAboutProjectSection,
    StyledMarathonPlanSection,
    StyledOfferPersonalAccountSection,
    StyledMarathonStepsSection,
    StyledPreFooterSection
} from "../components/index-page-styled";
import {fetchApi} from "../utilities/fetchApi";
import moment from "moment/moment";
import Link from "next/link";
import parse from "html-react-parser";
import {Header} from "../components/Header/Header";
import {Footer} from "../components/Footer/Footer";

const Index = props => {
    const {data} = props;
    const {
        topSection: {instagram_tag, image},
        homePage: {title, description, keywords, marathon_price, marathonStartDate},
        layout: {instagram_url, whats_app_url},
        aboutProjectSection: {left_header, left_text, right_header, right_text, middle_text, leftImage, rightImage, middleImage},
        bottomSection,
        customCode: {head}
    } = data;

    moment.locale('ru');
    const parsedMarathonStartDate = moment(marathonStartDate, "DD-MM-YYYY").format('DD MMMM');

    const router = useRouter();

    const handleRouteChange = url => {
        if (history.state.as !== url) {
            typeof Ya === 'function' && Ya.Metrika2.counters().forEach(({id}) => {
                ym(id, 'hit', url);
            });

            typeof ga === 'function' && ga.getAll().forEach(item => {
                gtag('config', item.b.data.values[':trackingId'], {page_path: url});
            });
        }
    };

    useEffect(() => {
        router.events.on('beforeHistoryChange', handleRouteChange);

        return () => {
            router.events.off('beforeHistoryChange', handleRouteChange);
        }
    }, []);

    useEffect(() => {
        let timeout;

        if (window.location.hash.length) {
            timeout = setTimeout(() => {
                window.scrollBy(0, -100)
            }, 0)
        }

        return () => {
            clearTimeout(timeout)
        }
    }, []);

    return (
        <div className='page-index'>
            <Head>
                <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
                <link rel="icon" type="image/x-icon" href="/assets/favicon.ico"/>
                <title>{title && title || ''}</title>
                <meta name="keywords" content={keywords && keywords || ''}/>
                <meta name="description" content={description && description || ''}/>
                {head && parse(`${head}`)}
            </Head>

            <Header/>

            <StyledBannerSection
                bg={image.cropped && image.cropped.url}
                id='main'>
                <div className="overlay"/>
                <h2 className='subtitle'>Старт {parsedMarathonStartDate}</h2>
                <h1 className='title'>Онлайн марафон</h1>
                <DariyaGolubkovaSvgFont className='author'/>
                <a href={instagram_url} target='_blank' rel='noopener noreferrer'
                   className="event-link">
                    <span className="event-link__svg-wrap"><InstagramIcon/></span>
                    <span className="event-link__text">{instagram_tag}</span>
                </a>
            </StyledBannerSection>

            <StyledAboutProjectSection
                id='about'
                backgroundImage={data.aboutProjectSection.backgroundImage.cropped
                    && data.aboutProjectSection.backgroundImage.cropped.url}
            >
                <div className="section-title">О проекте</div>

                <div className="section-item-wrap">
                    <div className="section-item">
                        <div className="img-wrap">
                            <img src={leftImage.cropped && leftImage.cropped.url} className="img" alt=''/>
                        </div>
                        <div className="text-wrap">
                            <p className='text'>{left_text}</p>
                            <h6 className='title'>{left_header}</h6>
                        </div>
                    </div>

                    <div className="section-item">
                        <div className="img-wrap">
                            <img src={rightImage.cropped && rightImage.cropped.url} className="img" alt=''/>
                        </div>
                        <div className="text-wrap">
                            <p className='text'>{right_text}</p>
                            <h6 className='title'>{right_header}</h6>
                        </div>
                    </div>
                </div>
            </StyledAboutProjectSection>

            <StyledMarathonPlanSection>
                <div className="section-title">Как проходит марафон</div>

                <div className="steps-wrap">
                    <div className="item">
                        <div className="item__svg-wrap"><CalendarIcon/></div>
                        <div className="item__text">Длительность одного&nbsp;марафона 4&nbsp;недели</div>
                    </div>
                    <div className="item">
                        <div className="item__svg-wrap"><WhatsappIcon/></div>
                        <div className="item__text">Онлайн поддержка и&nbsp;мой&nbsp;личный контроль в WhatsApp
                        </div>
                    </div>
                    <div className="item">
                        <div className="item__svg-wrap"><SaladIcon/></div>
                        <div className="item__text">5-7 раз в день <b>обязательные</b> фотоотчеты
                            о&nbsp;приемах&nbsp;пищи
                        </div>
                    </div>
                    <div className="item">
                        <div className="item__svg-wrap"><WomanIcon/></div>
                        <div className="item__text">3-5 раз в неделю <b>обязательные</b> занятия спортом, если нет
                            противопоказаний
                        </div>
                    </div>
                </div>
            </StyledMarathonPlanSection>

            <StyledOfferPersonalAccountSection>
                <div className="offer-bg">
                    <img src={middleImage.cropped && middleImage.cropped.url} alt=''/>
                </div>

                <div className="content">
                    <div className="features" dangerouslySetInnerHTML={{__html: middle_text}}/>
                </div>
            </StyledOfferPersonalAccountSection>

            <StyledMarathonStepsSection>
                <div className="section-title">Все просто!</div>

                <ul className="steps">
                    <li className="item">
                        <span className='item__number'>1</span>
                        <span className='item__text'>Оплати участие в марафоне</span>
                    </li>
                    <li className="item">
                        <span className='item__number'>2</span>
                        <span
                            className='item__text'>Получи доступ к&nbsp;закрытому чату и&nbsp;личному кабинету</span>
                    </li>
                    <li className="item">
                        <span className='item__number'>3</span>
                        <span className='item__text'>Питайся и тренируся, четко&nbsp;выполняй рекомендации</span>
                    </li>
                    <li className="item">
                        <span className='item__number'>4</span>
                        <span className='item__text'>Снижай вес без голоданий и вреда здоровью</span>
                    </li>
                </ul>

                <div className="cost-participation">
                    <span className='cost-participation__item'>Стоимость участия</span>
                    <span className='cost-participation__item'>
                        <span className="sup">{marathon_price}</span>
                        <span className='sub'>&nbsp;&nbsp;руб</span>
                        <span className="divider">&nbsp;|&nbsp;</span>
                        <span className="sup">4</span>
                        <span className="sub">&nbsp;&nbsp;недели</span></span>
                </div>

                <Button
                    as="a"
                    className='pay'
                    target='_blank'
                    href={whats_app_url}
                    rel='noopener noreferrer'
                    text={'Оплатить'}/>

                <div className='terms-agreement'>Кнопка перведет вас на менеджера в WhatsApp, который поможет
                    совершить
                    оплату и получить доступ к марафону.
                    Оплачивая участие вы соглашаетесь с <Link href="/terms-agreement"><a
                        target='_blank'>условиями</a></Link> участия в марафоне.
                </div>
            </StyledMarathonStepsSection>

            <ParticipationResultsSection/>

            <FeedbackSection/>

            <StyledPreFooterSection
                backgroundImage={data.bottomSection.backgroundImage.cropped
                    && data.bottomSection.backgroundImage.cropped.url}
            >
                <h6 className="section-title">{bottomSection.left_header}</h6>
                <div className="section-subtitle">Старт марафона {marathonStartDate}</div>

                <div className="cost-participation">
                    <span className='cost-participation__item'>Стоимость участия</span>
                    <div className='cost-participation__item'>
                        <span className="sup">{marathon_price}</span>
                        <span className='sub'>&nbsp;&nbsp;руб</span>
                        <span className="divider">&nbsp;&nbsp;|&nbsp;&nbsp;</span>
                        <span className="sup">4</span>
                        <span className="sub">&nbsp;&nbsp;недели</span>
                    </div>
                </div>

                <Button as="a"
                        className='pay'
                        target='_blank'
                        href={whats_app_url}
                        rel='noopener noreferrer'
                        text={'Оплатить'}/>

                <div
                    className='terms-agreement' dangerouslySetInnerHTML={{__html: bottomSection.left_text}}/>

                <div className="food-img-wrap"><img
                    src={bottomSection.leftImage.cropped && bottomSection.leftImage.cropped.url} alt=''/></div>
                <div className="author-photo-img-wrap"><img
                    src={bottomSection.rightImage.cropped && bottomSection.rightImage.cropped.url} alt=''/></div>

                <div className="get-in-touch">
                    <div className="get-in-touch__title">{bottomSection.right_header}</div>
                    <div className="get-in-touch__subtitle"
                         dangerouslySetInnerHTML={{__html: bottomSection.right_text}}/>
                    <Button as="a"
                            className='get-in-touch__link'
                            target='_blank'
                            href={whats_app_url}
                            rel='noopener noreferrer'
                            text={'Написать'}/>
                </div>
            </StyledPreFooterSection>
            <Footer/>
        </div>
    )
};

Index.propTypes = {
    data: PropTypes.object
};

Index.getInitialProps = async ({store}) => {
    const {fetchDataStore} = store;
    const {data} = await fetchApi('/landing');
    fetchDataStore.fetchData(data);
    return {data}
};

export default Index;