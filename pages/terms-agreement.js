import React from 'react';
import Head from "next/dist/next-server/lib/head";
import {StyledTermsAgreementPage} from "../components/terms-agreement-styled";
import {fetchApi} from "../utilities/fetchApi";
import {Header} from "../components/Header/Header";
import {Footer} from "../components/Footer/Footer";

const TermsAgreementPage = () => {

    return <>
        <Head>
            <title>Условия участия</title>
        </Head>

        <Header/>

        <StyledTermsAgreementPage>
            <h6 className="title"><span className="title__count">01.</span> Что такое марафон Дарьи Голубковой?</h6>
            <p className="paragraph">
                Марафон – это 4 недельная авторская программа Дарьи Голубковой, направленная в первую очередь на
                приобретение участниками навыков правильного питания и здорового образа жизни. При выполнении всех
                рекомендаций кураторов марафона участник снижают либо поддерживаю текущий вес в зависимости от цели
                участника. Рекомендации кураторов марафона содержат информацию о принципах правильного питания, вредном
                и
                полезном составе продуктов, запрещенных продуктах, а также о физической активности и кардио тренировках.
            </p>
            <p className="paragraph">
                Марафон проходит в режиме он-лайн. Каждый, оплативший участие, на время действия марафона получает
                доступ к
                закрытому личному кабинету на сайте марафона. В личном кабинете участник получает доступ к информации о
                питании и тренировках, а так же к разделу с рецептами. В личном кабинете на сайте участник может
                рассчитать
                индивидуальный калораж, оставлять замеры (вес, объемы) и видеть информацию о динамике измерений на
                наглядных
                графиках.
            </p>
            <p className="paragraph">
                Взаимодействие участников и кураторов происходит в групповом чате марафона в WhatsApp.
            </p>

            <h6 className="title"><span className="title__count">02.</span>Обязательными условиями участия в марафоне
                являются:</h6>
            <p className="paragraph">
                <span className='paragraph__count'>02.1.</span>
                Ежедневные фотоотчеты о приемах пищи в чате марафона. Куратор может в любой момент запросить информацию
                о
                весе порции и составе блюда, а участник будет обязан ее предоставить.
            </p>
            <p className="paragraph">
                <span className='paragraph__count'>02.2.</span>
                Обязательные кардио тренировки 3-5 раз в неделю. Участники обязаны присылать фотоотчет о тренировке в
                чат
                куратору. От тренировок освобождаются только те участники, у которых есть противопоказания по
                медицинским
                факторам.
            </p>
            <p className="paragraph">
                <span className='paragraph__count'>02.3.</span>
                Обязательный отказ от употребления алкоголя.
            </p>
            <p className="paragraph">
                <span className='paragraph__count'>02.4.</span>
                Еженедельные отчеты о весе и объемах в личном кабинете на сайте марафона.
            </p>

            <p className="paragraph">
                Участники, нарушающие любое из условий, могут быть удалены из чата без предупреждения. Таким участникам
                блокируется доступ к личному кабинету на сайте.
                В случае нарушения условий марафона заблокированному участнику не возвращаются денежные средства.
            </p>

            <p className="paragraph">
                Заблокированный участник может принять участие в следующих марафонах, заново оплатив стоимость участия.
            </p>

            <h6 className="title">Требования к техническим возможностям:</h6>
            <p className="paragraph">
                <span className='paragraph__count'>03.1.</span>
                Для участия в марафоне необходим доступ в Интернет каждый день.</p>
            <p className="paragraph">
                <span className='paragraph__count'>03.2.</span>
                Для участия в марафоне обязательно нужен WhatsApp, так как многие отчеты и обратную
                связь вы будете получать в чате участников марафона.</p>
            <p className="paragraph">
                <span className='paragraph__count'>03.3.</span>
                Для участия в марафоне обязательно необходим e-mail, так как доступ в личный кабинет
                без e-mail не возможен.</p>
            <p className="paragraph">
                <span className='paragraph__count'>03.4.</span>
                Для участия в марафоне необходимо иметь возможность раз в неделю взвешиваться, замерять
                обхват груди, бедер и талии, делать фото весов с показателями веса, а также свои фото в начале марафона
                и в
                конце марафона. Эти данные необходимо будет оставлять в личном кабинете на сайте марафона.</p>
            <p className="paragraph">
                <span className='paragraph__count'>03.5.</span>
                Для участия в марафоне необходимо установить бесплатное приложение FatSecret.</p>
        </StyledTermsAgreementPage>

        <Footer/>
    </>
};

TermsAgreementPage.getInitialProps = async ({store}) => {
    const {fetchDataStore} = store;
    const {data} = await fetchApi('/landing');
    fetchDataStore.fetchData(data);
    return {data}
};

export default TermsAgreementPage;