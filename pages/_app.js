import React from 'react';
import App from 'next/app';
import {configure} from 'mobx';
import {withMobx} from 'next-mobx-wrapper';
import {Provider, useStaticRendering} from 'mobx-react';
import {GlobalStyle} from "../globalStyles";

import * as getStores from '../stores';
import {CalculateViewportSizes} from "../services/calculateViewportSizes";

const isServer = !process.browser;

configure({enforceActions: 'observed'});
useStaticRendering(isServer);

class MyApp extends App {
    render() {
        const {Component, pageProps, store} = this.props;

        return (
            <Provider {...store}>
                <CalculateViewportSizes>
                    <GlobalStyle/>
                        <Component {...pageProps} />
                </CalculateViewportSizes>
            </Provider>
        )
    }
}

export default withMobx(getStores)(MyApp);